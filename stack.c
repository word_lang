#include <stdlib.h>

#include "stack.h"

/* According to some resources, some implementations of malloc are
   reluctant to allocate objects with a size larger than half the
   range of the maximum size, perhaps due to internal overflows in
   pointer comparisons.
*/
#ifdef SIZE_MAX
#  define SEGMENT_LIMIT (SIZE_MAX / 2)
#else
#  define SEGMENT_LIMIT (((size_t)(-1)) / 2)
#endif

#define INITIAL_SIZE 16

typedef struct stack_segment {
  int *stack_data;
  size_t allocated_size;
  size_t head_idx;
  struct stack_segment *next;
} stack_segment;

stack_segment *head = NULL;

static stack_segment *new_seg(stack_segment * link_target)
{
  stack_segment *seg = calloc(1, sizeof (stack_segment));

  seg->allocated_size = INITIAL_SIZE;
  seg->stack_data = malloc(INITIAL_SIZE * sizeof (int));
  seg->head_idx = 0;
  seg->next = link_target;
  return seg;
}

void stack_init()
{
  head = new_seg(NULL);
}

void stack_push(int val)
{
  if (head->head_idx >= head->allocated_size) {
    if (head->allocated_size * 2 > SEGMENT_LIMIT) {
      head = new_seg(head);
    } else {
      head->allocated_size *= 2;
      head->stack_data =
          realloc(head->stack_data, head->allocated_size * sizeof (int));
    }
  }

  head->stack_data[head->head_idx++] = val;
}

int stack_pop()
{
  stack_segment *new_head;

  if (head->head_idx == 0) {
    /*
     * this behavior is undefined.  Is interpreted as 'exit' 
     */
    if (head->next == NULL)
      exit(0);

    new_head = head->next;
    free(head->stack_data);
    free(head);
    head = new_head;

  }
  return head->stack_data[--head->head_idx];
}
