#ifndef _EXECUTE_H_
#  define _EXECUTE_H_

#  include "word.h"

void runtime_init();
void execute(word *);

#endif
