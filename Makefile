CC=gcc
CFLAGS=-O2 -Wall -Werror -ansi -pedantic -g
LDFLAGS=

all: word

word: main.o stack.o word.o execute.o
	$(CC) $(LDFLAGS) -o word main.o stack.o word.o execute.o

main.o: main.c stack.h word.h
	$(CC) $(CFLAGS) -c -o main.o main.c

execute.o: execute.c execute.h word.h
	$(CC) $(CFLAGS) -c -o execute.o execute.c

stack.o: stack.c stack.h
	$(CC) $(CFLAGS) -c -o stack.o stack.c

word.o: word.c word.h
	$(CC) $(CFLAGS) -c -o word.o word.c

clean:
	find -name '*.o' -delete;
	rm word
