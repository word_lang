#ifndef _STACK_H_
#  define _STACK_H_

void stack_init();

void stack_push(int);
int stack_pop();

#endif
