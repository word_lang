#ifndef _WORD_H_
#  define _WORD_H_

#  include <stdio.h>

typedef struct word {
  char *data;
  size_t storage_size;
  size_t word_length;
  char eof;
} word;

void free_word(word **);
void next_word(FILE *, word **);

#endif
