#include <stdlib.h>

#include "word.h"

#define INITIAL_WORD_STORAGE 4

static int comp(const void *a, const void *b)
{
  if (*(const char *) a == '.')
    return 1;
  if (*(const char *) b == '.')
    return -1;
  return *(const char *) a - *(const char *) b;
}

void free_word(word ** w)
{
  if (!w || !(*w))
    return;

  free((*w)->data);
  free(*w);
  *w = NULL;
}

void next_word(FILE * ifp, word ** out)
{
  int c;

  if (!ifp || !out)
    return;

  if (!(*out)) {
    *out = malloc(sizeof (word));
    (*out)->storage_size = INITIAL_WORD_STORAGE;
    (*out)->data = malloc(sizeof (char) * INITIAL_WORD_STORAGE);
  }

  (*out)->word_length = 0;

  while ((c = fgetc(ifp)) != EOF) {
    if (c >= 'A' && c <= 'Z')
      c += 'a' - 'A';

    if (c == '.' || (c >= 'a' && c <= 'z')) {
      if ((*out)->word_length + 1 >= (*out)->storage_size) {
        (*out)->storage_size *= 2;
        (*out)->data = realloc((*out)->data,
                               (*out)->storage_size * sizeof (char));
      }
      (*out)->data[(*out)->word_length++] = (char) c;
    } else if ((*out)->word_length > 0) {
      break;
    }
  }

  (*out)->eof = (c == EOF);
  qsort((*out)->data, (*out)->word_length, sizeof (char), comp);
}
