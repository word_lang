#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "execute.h"
#include "word.h"

int main(int argc, char **argv)
{
  word *w = NULL;
  FILE *ifp = NULL;

  if (argc != 2) {
    fprintf(stderr, "Usage: %s INPUT_FILE\n", argv[0]);
    return -1;
  }

  ifp = fopen(argv[1], "r");

  runtime_init();

  do {
    next_word(ifp, &w);
    execute(w);
  } while (!w->eof);

  free_word(&w);

  return 0;
}
