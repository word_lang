#include <stdio.h>
#include <string.h>

#include "execute.h"
#include "stack.h"

static int variables[18];

void runtime_init()
{
  memset(variables, 0, sizeof (int) * 18);
  stack_init();
}

#define NOT_LAST ((c + 1 != w->data + w->word_length) && *(c + 1) != '.')
#define NEXT_IS_VAR ((c + 1 != w->data + w->word_length) &&     \
                     *(c + 1) <= 'z' &&                         \
                     *(c+1) >= 'i')

void execute(word * w)
{
  char *c;
  int t1, t2;

  if (!w)
    return;

  for (c = w->data; c != w->data + w->word_length; ++c) {
    switch (*c) {
    case 'a':
    case 'n':
      stack_push(stack_pop() + stack_pop());
      break;
    case 'b':
    case 'o':
      t1 = stack_pop();
      t2 = stack_pop();
      stack_push(t1 - t2);
      break;
    case 'c':
    case 'p':
      stack_push(stack_pop() * stack_pop());
      break;
    case 'd':
    case 'q':
      t1 = stack_pop();
      t2 = stack_pop();
      stack_push(t1 / t2);
      break;
    case 'e':
    case 'r':
      break;
    case 'f':
    case 's':
      stack_push(w->word_length);
      break;
    case 'g':
    case 't':
      if (NOT_LAST)
        stack_push((int) *(c + 1));
      break;
    case 'h':
    case 'u':
      if (NEXT_IS_VAR) {
        variables[*(c + 1) - 'i'] = stack_pop();
      }
      break;
    case 'i':
    case 'v':
      if (NEXT_IS_VAR) {
        stack_push(variables[*(c + 1) - 'i']);
      }
      break;
    case 'j':
    case 'w':
      t1 = stack_pop();
      stack_push(t1);
      stack_push(t1);
      break;
    case 'k':
    case 'x':
      t1 = stack_pop();
      t2 = stack_pop();
      stack_push(t1);
      stack_push(t2);
      break;
    case 'l':
    case 'y':
      stack_push(fgetc(stdin));
      break;
    case 'm':
    case 'z':
      stack_pop();
      break;
    case '.':
      printf("%c", (char) stack_pop());
    default:
      break;
    }
  }
}
